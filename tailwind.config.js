const defaultTheme = require('tailwindcss/defaultTheme')
const textStylesPlugin = require('./src/utils/twTextStylesPlugin')

module.exports = {
  purge: ['./src/**/*.tsx'],
  mode: 'jit',
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        sans: ['"DM Sans"', ...defaultTheme.fontFamily.sans],
        poppins: ['Poppins', 'sans-serif'],
      },
      colors: {
        primary: {
          1: '#3772FF',
          '1-variant': '#2A55BF',
          2: '#9757D7',
          3: '#EF466F',
          4: '#45B26B',
        },
        secondary: {
          1: '#4BC9F0',
          2: '#E4D7CF',
          3: '#FFD166',
          4: '#CDB4DB',
        },
        neutrals: {
          1: '#141416',
          2: '#23262F',
          3: '#353945',
          4: '#777E90',
          5: '#B1B5C3',
          6: '#E6E8EC',
          7: '#F4F5F6',
          8: '#FCFCFD',
        },
      },
      boxShadow: {
        'depth-1': '0px 8px 16 -8px rgba(15, 15, 15, 0.2)',
        'depth-2': '0px 24px 24px -16px rgba(15, 15, 15, 0.2)',
        'depth-3': '0px 40px 32px -23px rgba(15, 15, 15, 0.12)',
        'depth-4': '0px 64px 64px -48px rgba(31, 47, 70, 0.12)',
        'depth-5': '0px 16px 64px -48px rgba(31, 47, 70, 0.4)',
      },
      screens: {
        tablet: '1024px',
        desktop: '1440px',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [textStylesPlugin, require('@tailwindcss/aspect-ratio')],
  corePlugins: {
    preflight: false,
  }
}
