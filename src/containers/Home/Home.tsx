import React from 'react'
import { Auction } from '@/components/Home/Auction/Auction'
import { ContentBlock } from '@/components/Home/ContentBlock/ContentBlock'
import { Feed } from '@/components/Home/Feed/Feed'

export const HomeContainer = () => {
  return (
    <div className="px-8 tablet:px-20 desktop:px-40">
      <ContentBlock />
      {/* <Auction /> */}
      <Feed />
    </div>
  )
}
