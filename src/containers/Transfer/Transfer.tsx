import React from 'react'
import {
  FormControl,
  FormLabel,
  Input,
  Button,
  useDisclosure,
  useToast,
  InputGroup,
  InputRightElement,
} from '@chakra-ui/react'
import { useForm } from 'react-hook-form'
import { PassphraseModal } from '@/components/Transfer/PassphraseModal'
import { useMutation } from 'react-query'
import { transfer, TransferParams } from '@/utils/transactions/transfer'
import { useAtom } from 'jotai'
import { nodeInfoAtom } from '@/atom/node-info'
import { sendTransactions } from '@/services/api'

export const TransferContainer = () => {
  const { isOpen, onOpen, onClose } = useDisclosure()
  const { register, handleSubmit, getValues, reset } = useForm()
  const [nodeInfo] = useAtom(nodeInfoAtom)
  const toast = useToast()
  const { mutate, isLoading } = useMutation(
    async (params: TransferParams) => {
      const res = await transfer(params)
      const sendResult = await sendTransactions(res.tx)
      return sendResult
    },
    {
      onSuccess() {
        reset()
        toast({
          title: 'Transfered.',
          status: 'success',
          duration: 9000,
          isClosable: true,
        })
      },
    }
  )
  const handleTransferClicked = (e) => {
    onOpen()
  }
  const handleTransfer = (passphrase: string) => {
    mutate({
      amount: getValues('amount'),
      recipientAddress: getValues('recipientAddress'),
      fee: getValues('fee'),
      minFeePerByte: nodeInfo.minFeePerByte,
      networkIdentifier: nodeInfo.networkIdentifier,
      passphrase,
    })
  }
  return (
    <div className="px-8 tablet:px-20 desktop:px-40">
      <div className="max-w-xl p-4 mx-auto ">
        <h4 className="headline-4">Transfer Funds</h4>
        <form className="p-4 mt-4 border rounded-md border-neutrals-6 shadow-depth-5">
          <FormControl id="recipient-address" isRequired mb={4}>
            <FormLabel>Recipient Address</FormLabel>
            <Input
              placeholder="Recipient Address"
              {...register('recipientAddress', { required: true })}
            />
          </FormControl>
          <FormControl id="amount" isRequired mb={4}>
            <FormLabel>Amount</FormLabel>
            <InputGroup>
              <Input
                placeholder="Amount"
                {...register('amount', { required: true })}
              />
              <InputRightElement>LSK</InputRightElement>
            </InputGroup>
          </FormControl>
          <FormControl id="fee" isRequired mb={4}>
            <FormLabel>Fee</FormLabel>
            <InputGroup>
              <Input
                placeholder="Fee"
                {...register('fee', { required: true })}
              />
              <InputRightElement>LSK</InputRightElement>
            </InputGroup>
          </FormControl>
          <PassphraseModal
            isOpen={isOpen}
            onClose={onClose}
            onSubmit={handleTransfer}
          />
          <Button
            onClick={handleSubmit(handleTransferClicked)}
            isLoading={isLoading}
          >
            Transfer
          </Button>
        </form>
      </div>
    </div>
  )
}
