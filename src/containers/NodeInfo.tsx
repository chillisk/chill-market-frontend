import React from 'react'
import { useAtom } from 'jotai'
import { nodeInfoAtom } from '@/atom/node-info'
import { fetchNodeInfo } from '@/services/api'
import { useQuery } from 'react-query'

export const NodeInfo: React.FC = ({ children }) => {
  const [, setNodeInfo] = useAtom(nodeInfoAtom)
  useQuery('get-node-info', () => fetchNodeInfo(), {
    onSuccess(info) {
      setNodeInfo({
        networkIdentifier: info.data.networkIdentifier,
        minFeePerByte: info.data.genesisConfig.minFeePerByte,
        height: info.data.height,
      })
    },
    refetchInterval: 1000,
  })
  return <React.Fragment>{children}</React.Fragment>
}
