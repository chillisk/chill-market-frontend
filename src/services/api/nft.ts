import { AxiosResponse } from 'axios'
import { nftApi } from './api'

export interface NFT {
  id: string
  value: string
  ownerAddress: string
  onSale: boolean
  name: string
  url: string
  tokenHistory: any[]
}

export interface ListNFT {
  data: NFT[]
}

export const getAllUserTokens = async (address: string): Promise<ListNFT> => {
  const resp: AxiosResponse<ListNFT> = await nftApi.get(
    `/${address}/nft_tokens`
  )
  return resp.data
}

export const getAllOnSaleTokens = async (): Promise<ListNFT> => {
  const resp: AxiosResponse<ListNFT> = await nftApi.get(`/nft_tokens/on_sale`)
  return resp.data
}
