import { AxiosResponse } from 'axios'
import { nftApi } from './api'

export interface Asset {
  amount: string
  recipientAddress: string
  data: string
}

export interface Datum {
  moduleID: number
  assetID: number
  nonce: string
  fee: string
  senderPublicKey: string
  asset: Asset
  signatures: string[]
  id: string
  amount: string
  recipientAddress: string
  data: string
  moduleName: string
  assetName: string
}

export interface TransactionData {
  data: Datum[]
}

export const getAllTransactions = async (): Promise<TransactionData> => {
  const resp: AxiosResponse<TransactionData> = await nftApi.get('/transactions')
  return resp.data
}
