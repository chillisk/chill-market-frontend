import { AxiosResponse } from 'axios'
import { blockchainApi } from './api'

export interface BaseFee {
  moduleID: number
  assetID: number
  baseFee: string
}

export interface Rewards {
  milestones: string[]
  offset: number
  distance: number
}

export interface GenesisConfig {
  blockTime: number
  communityIdentifier: string
  maxPayloadLength: number
  bftThreshold: number
  minFeePerByte: bigint
  baseFees: BaseFee[]
  rewards: Rewards
  minRemainingBalance: string
  activeDelegates: number
  standbyDelegates: number
  delegateListRoundOffset: number
}

export interface TransactionAsset {
  id: number
  name: string
}

export interface RegisteredModule {
  id: number
  name: string
  actions: string[]
  events: any[]
  reducers: string[]
  transactionAssets: TransactionAsset[]
}

export interface SeedPeer {
  ip: string
  port: number
}

export interface Network {
  port: number
  seedPeers: SeedPeer[]
}

export interface Data {
  version: string
  networkVersion: string
  networkIdentifier: string
  lastBlockID: string
  height: number
  finalizedHeight: number
  syncing: boolean
  unconfirmedTransactions: number
  genesisConfig: GenesisConfig
  registeredModules: RegisteredModule[]
  network: Network
}

export interface Meta {}

export interface NodeInfoData {
  data: Data
  meta: Meta
}

export const fetchNodeInfo = async (): Promise<NodeInfoData> => {
  const response: AxiosResponse<NodeInfoData> = await blockchainApi.get(
    '/node/info'
  )
  return response.data
}
