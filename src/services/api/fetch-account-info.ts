import { AxiosResponse } from 'axios'
import { blockchainApi } from './api'

export interface Token {
  balance: number
}

export interface Sequence {
  nonce: string
}

export interface Keys {
  numberOfSignatures: number
  mandatoryKeys: any[]
  optionalKeys: any[]
}

export interface Delegate {
  username: string
  pomHeights: any[]
  consecutiveMissedBlocks: number
  lastForgedHeight: number
  isBanned: boolean
  totalVotesReceived: string
}

export interface Dpos {
  delegate: Delegate
  sentVotes: any[]
  unlocking: any[]
}

export interface Nft {
  ownNFTs: any[]
}

export interface Data {
  address: string
  token: Token
  sequence: Sequence
  keys: Keys
  dpos: Dpos
  nft: Nft
}

export interface Meta {}

export interface AccountInfoData {
  data: Data
  meta: Meta
}

export const fetchAccountInfo = async (
  address: string
): Promise<AccountInfoData> => {
  const response: AxiosResponse<AccountInfoData> = await blockchainApi.get(
    `accounts/${address}`
  )
  return response.data
}
