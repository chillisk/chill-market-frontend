import { AxiosResponse } from 'axios'
import { blockchainApi } from './api'

export interface Data {
  transactionId: string
}

export interface Meta {}

export interface TransactionData {
  data: Data
  meta: Meta
}

export const sendTransactions = async (
  tx: object
): Promise<TransactionData> => {
  const response: AxiosResponse<TransactionData> = await blockchainApi.post(
    'transactions',
    tx
  )

  return response.data
}
