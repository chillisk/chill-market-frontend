import axios from 'axios'

export const blockchainApi = axios.create({
  baseURL: `${process.env.NEXT_PUBLIC_BLOCKCHAIN_APP}/api`,
})

export const nftApi = axios.create({
  baseURL: `${process.env.NEXT_PUBLIC_NFT_APP}/api`,
})
