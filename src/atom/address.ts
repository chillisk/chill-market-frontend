import { atom } from 'jotai'
import { cryptography } from '@liskhq/lisk-client'

const passphraseAtom = atom('')
const base32AddressAtom = atom((get) =>
  cryptography.getBase32AddressFromPassphrase(get(passphraseAtom)).toString()
)

const userInfoAtom = atom({
  address: '',
  base32Address: '',
})

export { passphraseAtom, base32AddressAtom, userInfoAtom }
