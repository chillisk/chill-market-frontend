import { atom } from 'jotai'

export const nodeInfoAtom = atom({
  networkIdentifier: '',
  minFeePerByte: BigInt(0),
  height: 0,
})
