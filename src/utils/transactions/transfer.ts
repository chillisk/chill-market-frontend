import { fetchAccountInfo } from '@/services/api'
import { transactions, codec, cryptography } from '@liskhq/lisk-client'
import { Schema } from '@liskhq/lisk-codec'
import { calcMinTxFee, getFullAssetSchema } from '../common'

export const transferAssetSchema = {
  $id: 'lisk/transfer-asset',
  title: 'Transfer transaction asset',
  type: 'object',
  required: ['amount', 'recipientAddress', 'data'],
  properties: {
    amount: {
      dataType: 'uint64',
      fieldNumber: 1,
    },
    recipientAddress: {
      dataType: 'bytes',
      fieldNumber: 2,
      minLength: 20,
      maxLength: 20,
    },
    data: {
      dataType: 'string',
      fieldNumber: 3,
      minLength: 0,
      maxLength: 64,
    },
  },
}

export interface TransferParams {
  recipientAddress: string
  amount: string
  passphrase: string
  fee: string
  networkIdentifier: string
  minFeePerByte: bigint
}

export const transfer = async ({
  recipientAddress,
  amount,
  passphrase,
  fee,
  networkIdentifier,
  minFeePerByte,
}: TransferParams) => {
  const { publicKey } =
    cryptography.getPrivateAndPublicKeyFromPassphrase(passphrase)
  const address = cryptography.getAddressFromPassphrase(passphrase)
  const {
    data: {
      sequence: { nonce },
    },
  } = await fetchAccountInfo(address.toString('hex'))
  const recipient = cryptography.getAddressFromBase32Address(recipientAddress)
  const { id, ...rest } = transactions.signTransaction(
    transferAssetSchema,
    {
      moduleID: 2,
      assetID: 0,
      nonce: BigInt(nonce),
      fee: BigInt(transactions.convertLSKToBeddows(fee)),
      senderPublicKey: publicKey,
      asset: {
        amount: BigInt(transactions.convertLSKToBeddows(amount)),
        recipientAddress: recipient,
        data: '',
      },
    },
    Buffer.from(networkIdentifier, 'hex'),
    passphrase
  )

  return {
    id: (id as Buffer).toString('hex'),
    tx: codec.codec.toJSON(
      getFullAssetSchema(transferAssetSchema) as Schema,
      rest
    ),
    minFee: calcMinTxFee(transferAssetSchema, minFeePerByte, rest),
  }
}
