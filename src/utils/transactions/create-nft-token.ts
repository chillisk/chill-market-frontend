import { fetchAccountInfo } from '@/services/api'
import { transactions, codec, cryptography } from '@liskhq/lisk-client'
import { Schema } from '@liskhq/lisk-codec'
import { calcMinTxFee, getFullAssetSchema } from '../common'

export const createNFTTokenSchema = {
  $id: 'lisk/nft/create',
  type: 'object',
  required: ['name', 'url'],
  properties: {
    name: {
      dataType: 'string',
      fieldNumber: 1,
    },
    url: {
      dataType: 'string',
      fieldNumber: 2,
    },
  },
}

export interface CreateNFTParams {
  name: string
  url: string
  passphrase: string
  networkIdentifier: string
  minFeePerByte: bigint
}

export const createNFTToken = async ({
  name,
  url,
  passphrase,
  networkIdentifier,
  minFeePerByte,
}: CreateNFTParams) => {
  const { publicKey } =
    cryptography.getPrivateAndPublicKeyFromPassphrase(passphrase)
  const address = cryptography.getAddressFromPassphrase(passphrase)
  const {
    data: {
      sequence: { nonce },
    },
  } = await fetchAccountInfo(address.toString('hex'))

  const { id, ...rest } = transactions.signTransaction(
    createNFTTokenSchema,
    {
      moduleID: 1024,
      assetID: 3,
      nonce: BigInt(nonce),
      fee: BigInt(transactions.convertLSKToBeddows('1')),
      senderPublicKey: publicKey,
      asset: {
        name,
        url,
      },
    },
    Buffer.from(networkIdentifier, 'hex'),
    passphrase
  )

  return {
    id: (id as Buffer).toString('hex'),
    tx: codec.codec.toJSON(
      getFullAssetSchema(createNFTTokenSchema) as Schema,
      rest
    ),
    minFee: calcMinTxFee(createNFTTokenSchema, minFeePerByte, rest),
  }
}
