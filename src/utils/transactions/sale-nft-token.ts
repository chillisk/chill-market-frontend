import { fetchAccountInfo } from '@/services/api'
import { transactions, codec, cryptography } from '@liskhq/lisk-client'
import { Schema } from '@liskhq/lisk-codec'
import { calcMinTxFee, getFullAssetSchema } from '../common'

export const saleNFTTokenSchema = {
  $id: 'lisk/nft/sale',
  type: 'object',
  required: ['nftId', 'saleValue'],
  properties: {
    nftId: {
      dataType: 'bytes',
      fieldNumber: 1,
    },
    saleValue: {
      dataType: 'uint64',
      fieldNumber: 2,
    },
  },
}

export interface SaleNFTParams {
  nftId: string
  saleValue: number
  passphrase: string
  networkIdentifier: string
  minFeePerByte: bigint
}

export const saleNFTToken = async ({
  nftId,
  saleValue,
  passphrase,
  networkIdentifier,
  minFeePerByte,
}: SaleNFTParams) => {
  const { publicKey } =
    cryptography.getPrivateAndPublicKeyFromPassphrase(passphrase)
  const address = cryptography.getAddressFromPassphrase(passphrase)
  const {
    data: {
      sequence: { nonce },
    },
  } = await fetchAccountInfo(address.toString('hex'))

  const { id, ...rest } = transactions.signTransaction(
    saleNFTTokenSchema,
    {
      moduleID: 1024,
      assetID: 1,
      nonce: BigInt(nonce),
      fee: BigInt(transactions.convertLSKToBeddows('1')),
      senderPublicKey: publicKey,
      asset: {
        nftId: Buffer.from(nftId, 'hex'),
        saleValue: BigInt(transactions.convertLSKToBeddows(saleValue + '')),
      },
    },
    Buffer.from(networkIdentifier, 'hex'),
    passphrase
  )

  return {
    id: (id as Buffer).toString('hex'),
    tx: codec.codec.toJSON(
      getFullAssetSchema(saleNFTTokenSchema) as Schema,
      rest
    ),
    minFee: calcMinTxFee(saleNFTTokenSchema, minFeePerByte, rest),
  }
}
