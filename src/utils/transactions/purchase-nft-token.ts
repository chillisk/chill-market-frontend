import { fetchAccountInfo } from '@/services/api'
import { transactions, codec, cryptography } from '@liskhq/lisk-client'
import { Schema } from '@liskhq/lisk-codec'
import { calcMinTxFee, getFullAssetSchema } from '../common'

export const purchaseNFTTokenSchema = {
  $id: 'lisk/nft/purchase',
  type: 'object',
  required: ['nftId', 'purchaseValue'],
  properties: {
    nftId: {
      dataType: 'bytes',
      fieldNumber: 1,
    },
    purchaseValue: {
      dataType: 'uint64',
      fieldNumber: 2,
    },
  },
}

export interface PurchaseNFTParams {
  nftId: string
  purchaseValue: number
  passphrase: string
  networkIdentifier: string
  minFeePerByte: bigint
}

export const purchaseNFTToken = async ({
  nftId,
  purchaseValue,
  passphrase,
  networkIdentifier,
  minFeePerByte,
}: PurchaseNFTParams) => {
  const { publicKey } =
    cryptography.getPrivateAndPublicKeyFromPassphrase(passphrase)
  const address = cryptography.getAddressFromPassphrase(passphrase)
  const {
    data: {
      sequence: { nonce },
    },
  } = await fetchAccountInfo(address.toString('hex'))

  const { id, ...rest } = transactions.signTransaction(
    purchaseNFTTokenSchema,
    {
      moduleID: 1024,
      assetID: 2,
      nonce: BigInt(nonce),
      fee: BigInt(transactions.convertLSKToBeddows('1')),
      senderPublicKey: publicKey,
      asset: {
        nftId: Buffer.from(nftId, 'hex'),
        purchaseValue: BigInt(
          transactions.convertLSKToBeddows(purchaseValue + '')
        ),
      },
    },
    Buffer.from(networkIdentifier, 'hex'),
    passphrase
  )

  return {
    id: (id as Buffer).toString('hex'),
    tx: codec.codec.toJSON(
      getFullAssetSchema(purchaseNFTTokenSchema) as Schema,
      rest
    ),
    minFee: calcMinTxFee(purchaseNFTTokenSchema, minFeePerByte, rest),
  }
}
