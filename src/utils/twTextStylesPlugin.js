const plugin = require('tailwindcss/plugin')

module.exports = plugin(function ({ addComponents, theme }) {
  const textStyles = {
    '.hero': {
      fontWeight: 'bold',
      fontSize: '96px',
      lineHeight: '96px',
      letterSpacing: '-0.02em',
    },
    '.headline-1': {
      fontWeight: 'bold',
      fontSize: '64px',
      lineHeight: '64px',
      letterSpacing: '-0.02em',
    },
    '.headline-2': {
      fontWeight: 'bold',
      fontSize: '48px',
      lineHeight: '56px',
      letterSpacing: '-0.02em',
    },
    '.headline-3': {
      fontWeight: 'bold',
      fontSize: '40px',
      lineHeight: '48px',
      letterSpacing: '-0.01em',
    },
    '.headline-4': {
      fontWeight: 'bold',
      fontSize: '32px',
      lineHeight: '40px',
      letterSpacing: '-0.01em',
    },
    '.body-1': {
      fontFamily: theme('fontFamily.poppins'),
      fontSize: '24px',
      lineHeight: '32px',
      letterSpacing: '-0.01em',
    },
    '.body-1-bold': {
      fontFamily: theme('fontFamily.poppins'),
      fontWeight: 600,
      fontSize: '24px',
      lineHeight: '32px',
    },
    '.body-2': {
      fontFamily: theme('fontFamily.poppins'),
      fontSize: '16px',
      lineHeight: '24px',
    },
    '.body-2-bold': {
      fontFamily: theme('fontFamily.poppins'),
      fontWeight: 500,
      fontSize: '16px',
      lineHeight: '24px',
    },
    '.caption': {
      fontFamily: theme('fontFamily.poppins'),
      fontSize: '14px',
      lineHeight: '24px',
    },
    '.caption-bold': {
      fontFamily: theme('fontFamily.poppins'),
      fontWeight: 500,
      fontSize: '14px',
      lineHeight: '24px',
    },
    '.caption-2': {
      fontFamily: theme('fontFamily.poppins'),
      fontSize: '12px',
      lineHeight: '20px',
    },
    '.caption-2-bold': {
      fontFamily: theme('fontFamily.poppins'),
      fontWeight: 600,
      fontSize: '12px',
      lineHeight: '20px',
    },
    '.hairline-1': {
      fontFamily: theme('fontFamily.poppins'),
      fontWeight: 'bold',
      fontSize: '16px',
      lineHeight: '16px',
      textTransform: 'uppercase',
    },
    '.hairline-2': {
      fontFamily: theme('fontFamily.poppins'),
      fontWeight: 'bold',
      fontSize: '12px',
      lineHeight: '12px',
      textTransform: 'uppercase',
    },
    '.button-1': {
      fontWeight: 'bold',
      fontSize: '16px',
      lineHeight: '16px',
    },
    '.button-2': {
      fontWeight: 'bold',
      fontSize: '14px',
      lineHeight: '16px',
    },
  }

  addComponents(textStyles)
})
