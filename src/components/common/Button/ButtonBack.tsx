import { ArrowBack } from '@/assets/icons/icons'
import { Button } from '@/components/shared/Button/Button'
import React from 'react'
import SVG from 'react-inlinesvg'

export const ButtonBack = () => {
  return (
    <Button className="flex item-center" variant="light">
      <SVG src={ArrowBack.src} className="w-4 h-4" />
      <div className="ml-[12px]">Back to home</div>
    </Button>
  )
}
