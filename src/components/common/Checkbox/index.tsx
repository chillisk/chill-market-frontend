import React from 'react'

interface CheckboxProps {
  title: string
  checked?: boolean
}

export const Checkbox: React.FC<CheckboxProps> = ({
  title,
  checked = false,
}) => {
  return (
    <div className="flex items-center mt-6">
      <input type="checkbox" checked={checked} className="w-5 h-5" />
      <span className="ml-[12px] caption-bold">{title}</span>
    </div>
  )
}
