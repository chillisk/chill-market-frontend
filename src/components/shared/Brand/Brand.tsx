import Link from 'next/link'
import React from 'react'
import { Logo } from './Logo'

export const Brand = () => {
  return (
    <Link href="/" passHref>
      <a className="flex items-center">
        <Logo />
        <span className="ml-2 font-poppins text-[#23262F] text-[24px] font-semibold">
          chill market
        </span>
      </a>
    </Link>
  )
}
