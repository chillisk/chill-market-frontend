import React from 'react'
import Image from 'next/image'
import logoImg from '@/assets/images/logo-s.png'

export const Logo = () => {
  return <Image src={logoImg} alt="logo" width={32} height={32} />
}
