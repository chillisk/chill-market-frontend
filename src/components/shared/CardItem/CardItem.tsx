import { NFT } from '@/services/api/nft'
import {
  Badge,
  Button,
  FormControl,
  FormLabel,
  Input,
  Modal,
  useDisclosure,
  Portal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
} from '@chakra-ui/react'
import React from 'react'
import Avatar from 'boring-avatars'
import { cryptography, transactions } from '@liskhq/lisk-client'
import { useForm } from 'react-hook-form'
import { useAtom } from 'jotai'
import { nodeInfoAtom } from '@/atom/node-info'
import { useMutation, useQueryClient } from 'react-query'
import {
  SaleNFTParams,
  saleNFTToken,
} from '@/utils/transactions/sale-nft-token'
import { sendTransactions } from '@/services/api'
import { GetAllUserTokens } from '@/pages/items'

interface CardItemProps {
  token: NFT
}

const CardItem: React.FC<CardItemProps> = ({ token }) => {
  const { isOpen, onClose, onOpen } = useDisclosure()
  const { handleSubmit, register, reset } = useForm()
  const [nodeInfo] = useAtom(nodeInfoAtom)
  const client = useQueryClient()
  const { mutate, isLoading } = useMutation(
    async (e: any) => {
      const data: SaleNFTParams = {
        passphrase: e.passphrase,
        saleValue: e.value,
        nftId: token.id,
        minFeePerByte: nodeInfo.minFeePerByte,
        networkIdentifier: nodeInfo.networkIdentifier,
      }
      const res = await saleNFTToken(data)
      const saleResult = await sendTransactions(res.tx)
      return saleResult
    },
    {
      onSuccess() {
        onClose()
        reset()
        client.invalidateQueries(GetAllUserTokens)
      },
    }
  )
  const onConfirm = (e) => {
    mutate(e)
  }
  return (
    <div>
      <div className="">
        <img
          src={token.url}
          alt="img"
          className="card-img w-full max-h-[303px] rounded-2xl overflow-hidden object-cover"
        />
        <div className="flex items-center justify-between mt-5">
          <h3 className="body-2-bold">{token.name}</h3>
          {token.onSale ? (
            <Badge colorScheme="green">
              {transactions.convertBeddowsToLSK(token.value)} LSK
            </Badge>
          ) : (
            <Badge>NON SALE</Badge>
          )}
        </div>
        <div className="flex justify-between mt-3">
          <div className="flex">
            <Avatar
              size={24}
              name={token.ownerAddress}
              variant="marble"
              colors={['#92A1C6', '#146A7C', '#F0AB3D', '#C271B4', '#C20D90']}
            />
            <div className="ml-2 text-neutrals-4 caption">
              {cryptography
                .getBase32AddressFromAddress(Buffer.from(token.ownerAddress))
                .toString()
                .substr(0, 15)}
              ...
            </div>
          </div>
          {!token.onSale ? (
            <Button size="xs" onClick={onOpen}>
              Sell Now
            </Button>
          ) : null}
          <Modal isOpen={isOpen} onClose={onClose} isCentered>
            <form onSubmit={handleSubmit(onConfirm)}>
              <ModalOverlay />
              <ModalContent>
                <ModalHeader>
                  Sell <span className="text-primary-1">{token.name}</span> NFT
                </ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                  <FormControl id="value" isRequired>
                    <FormLabel>Value</FormLabel>
                    <Input
                      type="number"
                      placeholder="Enter NFT value"
                      {...register('value', { required: true })}
                    />
                  </FormControl>
                  <FormControl id="passphrase" isRequired className="mt-4">
                    <FormLabel>Passphrase</FormLabel>
                    <Input
                      placeholder="Enter your passphrase"
                      {...register('passphrase', { required: true })}
                    />
                  </FormControl>
                </ModalBody>

                <ModalFooter>
                  <Button variant="ghost" mr={3} onClick={onClose}>
                    Cancel
                  </Button>
                  <Button
                    colorScheme="blue"
                    type="submit"
                    isLoading={isLoading}
                  >
                    Confirm
                  </Button>
                </ModalFooter>
              </ModalContent>
            </form>
          </Modal>
        </div>
      </div>
    </div>
  )
}

export default CardItem
