import React from 'react'
import clsx from 'clsx'

interface NavItemProps {
  active?: boolean
}

export const NavItem: React.FC<NavItemProps> = ({ children, active }) => {
  return (
    <div
      className={clsx(
        'transition text-sm font-bold leading-4 cursor-pointer hover:text-neutrals-1',
        active ? 'text-neutrals-2' : 'text-neutrals-4'
      )}
    >
      {children}
    </div>
  )
}
