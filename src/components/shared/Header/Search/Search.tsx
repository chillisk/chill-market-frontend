import React from 'react'
import SVG from 'react-inlinesvg'
import clsx from 'clsx'
import { SearchIcon } from '@/assets/icons/icons'
interface SearchProps {
  placeholder: string
  className?: string
}

export const Search: React.FC<SearchProps> = ({
  placeholder = '',
  className,
}) => {
  return (
    <div
      className={clsx(
        'relative text-[12px] font-poppins leading-5 text-neutrals-4',
        className
      )}
    >
      <input
        className="border-2 w-full min-w-[256px] border-solid border-neutrals-6 placeholder-neutrals-4 px-4 py-[10px] rounded-lg focus:outline-none focus:border-blue-500 transition"
        placeholder={placeholder}
      ></input>
      <SVG src={SearchIcon.src} className="absolute right-3 top-3" />
    </div>
  )
}
