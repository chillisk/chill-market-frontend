import React from 'react'
import clsx from 'clsx'

interface NavProps extends React.BaseHTMLAttributes<HTMLDivElement> {}

export const Nav: React.FC<NavProps> = ({ children, className }) => {
  return (
    <div className={clsx('hidden desktop:flex xl:gap-8', className)}>
      {children}
    </div>
  )
}
