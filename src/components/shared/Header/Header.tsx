import React, { Fragment, useState } from 'react'
import Link from 'next/link'
import SVG from 'react-inlinesvg'
import { Popover, Transition, Dialog } from '@headlessui/react'
import { BurgerIcon, CloseIcon } from '@/assets/icons/icons'
import { Search } from './Search/Search'
import { Brand } from '../Brand/Brand'
import { Nav } from './Nav'
import { NavItem } from './NavItem'
import { Notification } from './Notification'
import { Button } from '../Button/Button'
import { NavMobileItem } from './NavMobileItem'
import { Dropdown } from '../Dropdown/Dropdown'
import { useAtom } from 'jotai'
import { base32AddressAtom, passphraseAtom, userInfoAtom } from '@/atom/address'
import { passphrase as listPassphrase, cryptography } from '@liskhq/lisk-client'

export const Header = () => {
  const [isOpen, setIsOpen] = useState(false)
  const [passphrase, setPassphrase] = useAtom(passphraseAtom)
  const [base32Address] = useAtom(base32AddressAtom)
  const [userInfo, setUserInfo] = useAtom(userInfoAtom)

  function createAccount() {
    const pw = listPassphrase.Mnemonic.generateMnemonic()
    setPassphrase(pw)
  }

  function connect() {
    const address = cryptography
      .getAddressFromBase32Address(base32Address)
      .toString('hex')
    setUserInfo({
      address,
      base32Address,
    })
    setPassphrase('')
    setIsOpen(false)
  }

  return (
    <>
      <Popover>
        {({ open }) => (
          <div className="flex items-center px-8 pt-12 pb-6 border-b-0 border-solid tablet:border-b tablet:pt-6 bg-neutrals-8 border-neutrals-6 tablet:px-20 desktop:px-40">
            <Brand />
            <div className="hidden desktop:block w-[2px] h-10 rounded-sm bg-neutrals-6 mx-8"></div>
            <Nav>
              <Link href="/" passHref>
                <a>
                  <NavItem>Discover</NavItem>
                </a>
              </Link>
              <Link href="/transfer" passHref>
                <a>
                  <NavItem>Transfer</NavItem>
                </a>
              </Link>
              <Link href="/transactions" passHref>
                <a>
                  <NavItem>Transactions</NavItem>
                </a>
              </Link>
            </Nav>
            <div className="hidden gap-6 ml-auto tablet:flex nav-right">
              <Search placeholder="Search" />
              <Notification hasNewNotification />
              <div className="flex gap-3 btn-group">
                <Link href="/upload" passHref>
                  <a>
                    <Button variant="neutral">Upload</Button>
                  </a>
                </Link>
                {userInfo.address ? (
                  <Dropdown />
                ) : (
                  <Button onClick={() => setIsOpen(true)}>
                    Connect Wallet
                  </Button>
                )}
              </div>
            </div>
            <div className="block ml-auto tablet:hidden">
              <Popover.Button className="inline-flex items-center justify-center p-2 text-gray-400 bg-white rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-primary-1">
                <SVG src={BurgerIcon.src} />
              </Popover.Button>
            </div>
            <Transition
              show={open}
              as={React.Fragment}
              enter="duration-200 ease-out"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="duration-100 ease-in"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <Popover.Panel
                focus
                static
                className="absolute inset-x-0 top-0 z-30 p-2 transition origin-top-right transform tablet:hidden"
              >
                <div className="bg-white divide-y-2 rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 divide-gray-50">
                  <div className="p-4">
                    <div className="flex w-full">
                      <Brand />
                      <div className="flex gap-2 ml-auto">
                        <Notification hasNewNotification />
                        <div className="w-[1px] h-full bg-neutrals-6 ml-2"></div>
                        <Popover.Button className="inline-flex items-center justify-center p-2 text-gray-400 bg-white rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-primary-1">
                          <span className="sr-only">Close menu</span>
                          <SVG
                            className="w-6 h-6"
                            aria-hidden="true"
                            src={CloseIcon.src}
                          />
                        </Popover.Button>
                      </div>
                    </div>
                    <div className="gap-4 mt-2 tablet:grid tablet:grid-cols-2 btn-group">
                      <Link href="/" passHref>
                        <a>
                          <NavMobileItem>Discover</NavMobileItem>
                        </a>
                      </Link>
                      <Link href="/transfer" passHref>
                        <a>
                          <NavMobileItem>Transfer</NavMobileItem>
                        </a>
                      </Link>
                      <Link href="/transactions" passHref>
                        <a>
                          <NavMobileItem>Transactions</NavMobileItem>
                        </a>
                      </Link>
                    </div>
                    <Search placeholder="Search" className="mt-4" />
                    <div className="flex justify-end gap-4">
                      <Link href="/upload" passHref>
                        <a>
                          <Button className="mt-4" variant="neutral">
                            Upload
                          </Button>
                        </a>
                      </Link>
                      <Button className="mt-4" onClick={() => setIsOpen(true)}>
                        Connect Wallet
                      </Button>
                    </div>
                  </div>
                </div>
              </Popover.Panel>
            </Transition>
          </div>
        )}
      </Popover>
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog
          className="fixed inset-0 z-10 overflow-y-auto"
          onClose={() => setIsOpen(false)}
        >
          <div className="flex items-center justify-center min-h-screen">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0 bg-black opacity-30" />
            </Transition.Child>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <div className="z-20 max-w-md p-8 mx-auto bg-white rounded shadow-depth-2">
                <Dialog.Title>
                  <div className="flex justify-between">
                    <h4 className="headline-4">Connect Wallet</h4>
                    <button
                      className="inline-flex items-center justify-center p-2 text-gray-400 bg-white rounded-md hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-primary-1"
                      onClick={() => setIsOpen(false)}
                    >
                      <span className="sr-only">Close menu</span>
                      <SVG
                        className="w-6 h-6"
                        aria-hidden="true"
                        src={CloseIcon.src}
                      />
                    </button>
                  </div>
                </Dialog.Title>

                <form className="mt-4">
                  <label className="caption-bold text-neutrals-4">
                    Passphrase
                  </label>
                  <input
                    placeholder="Your passphrase"
                    value={passphrase}
                    className="border-2 w-full min-w-[300px] border-solid border-neutrals-6 placeholder-neutrals-4 px-4 py-[10px] rounded-lg focus:outline-none focus:border-blue-500 transition"
                    onChange={(e) => setPassphrase(e.target.value)}
                  />
                  {passphrase ? (
                    <div className="my-4">
                      <h4 className="caption-bold text-neutrals-4">
                        Your address
                      </h4>
                      <div className="p-1 border border-dashed border-primary-3">
                        {base32Address}
                      </div>
                    </div>
                  ) : null}
                </form>

                <div className="flex justify-between mt-8">
                  <Button onClick={createAccount}>Create Account</Button>
                  <Button variant="neutral" onClick={connect}>
                    Connect
                  </Button>
                </div>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition>
    </>
  )
}
