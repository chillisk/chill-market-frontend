import React from 'react'
import { BellIcon } from '@/assets/icons/icons'
import SVG from 'react-inlinesvg'

interface NotificationProps {
  hasNewNotification?: boolean
}

export const Notification: React.FC<NotificationProps> = ({
  hasNewNotification,
}) => {
  return (
    <div className="relative p-2 text-neutrals-4">
      <SVG src={BellIcon.src} />
      {hasNewNotification ? (
        <div className="absolute top-0 right-0 w-3 h-3 rounded-lg bg-primary-4"></div>
      ) : null}
    </div>
  )
}
