import React from 'react'
import SVG from 'react-inlinesvg'
import { ArrowDownIcon, SearchIcon } from '@/assets/icons/icons'
import clsx from 'clsx'

interface SearchProps {
  title?: string
  value: string
  className?: string
}

export const Select: React.FC<SearchProps> = ({
  title = '',
  className,
  value,
}) => {
  return (
    <div
      className={clsx(
        'text-[12px] font-poppins leading-5 text-neutrals-4',
        className
      )}
    >
      {title ? (
        <div className="mb-[12px] uppercase hairline-2">{title}</div>
      ) : null}
      <div className="relative">
        <div className="border-2 border-solid border-neutrals-6 placeholder-neutrals-4 px-4 py-[10px] rounded-lg">
          {value}
        </div>
        <div className="absolute right-3 top-2.5">
          <div className="flex items-center justify-center w-6 h-6 ml-2 transition border-2 rounded-full cursor-pointer border-neutrals-6 hover:bg-neutrals-7">
            <SVG src={ArrowDownIcon.src} />
          </div>
        </div>
      </div>
    </div>
  )
}
