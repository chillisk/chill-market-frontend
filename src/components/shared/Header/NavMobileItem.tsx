import React from 'react'
import clsx from 'clsx'

interface NavMobileItemProps {
  active?: boolean
}

export const NavMobileItem: React.FC<NavMobileItemProps> = ({
  children,
  active,
}) => {
  return (
    <div
      className={clsx(
        'transition font-poppins text-sm font-semibold text-neutrals-3 rounded-md leading-4 cursor-pointer hover:text-neutrals-1 p-3',
        active ? 'bg-neutrals-7 text-neutrals-2' : ''
      )}
    >
      {children}
    </div>
  )
}
