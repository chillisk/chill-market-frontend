import {
  BalanceIcon,
  DarkThemeIcon,
  LiskIcon,
  MyItemIcon,
  ProfileIcon,
  SignOutIcon,
  TokenIcon,
} from '@/assets/icons/icons'
import { userInfoAtom } from '@/atom/address'
import { Popover, Transition } from '@headlessui/react'
import { useAtom } from 'jotai'
import React from 'react'
import SVG from 'react-inlinesvg'
import Avatar from 'boring-avatars'
import { Divided } from '../Divided/Divided'
import { useQuery } from 'react-query'
import { fetchAccountInfo } from '@/services/api'
import Link from 'next/link'
import { transactions } from '@liskhq/lisk-client'

export const Dropdown = () => {
  const [userInfo, setUserInfo] = useAtom(userInfoAtom)
  function disconnect() {
    setUserInfo({
      base32Address: '',
      address: '',
    })
  }
  const { data, isLoading } = useQuery(
    ['fetch-data-acc', userInfo.address],
    () => {
      return fetchAccountInfo(userInfo.address)
    }
  )
  return (
    <Popover className="relative">
      <Popover.Button className="outline-none focus:outline-none">
        <div className="flex items-center border-2 border-solid button-2  border-neutrals-6 rounded-[90px]">
          <div className="ml-1">
            <Avatar
              size={32}
              name={userInfo.address}
              variant="marble"
              colors={['#92A1C6', '#146A7C', '#F0AB3D', '#C271B4', '#C20D90']}
            />
          </div>
          <div className="p-[12px]">
            <span className="text-neutrals-2">
              {data
                ? transactions.convertBeddowsToLSK(data.data.token.balance + '')
                : 0}
            </span>
            <span className="ml-1 text-primary-4">LSK</span>
          </div>
        </div>
      </Popover.Button>
      <Transition
        enter="transition duration-100 ease-out"
        enterFrom="transform scale-95 opacity-0"
        enterTo="transform scale-100 opacity-100"
        leave="transition duration-75 ease-out"
        leaveFrom="transform scale-100 opacity-100"
        leaveTo="transform scale-95 opacity-0"
      >
        <Popover.Panel className="absolute z-10 w-[256px] right-0 mt-6 px-4 py-8 bg-white shadow-depth-5 rounded-[12px]">
          <div className="absolute -top-5 left-[108px] w-[0px] h-[0px] border-l-[20px] border-r-[20px] border-b-[20px] border-l-solid border-r-solid border-b-solid border-l-transparent border-r-transparent border-b-white"></div>
          <div className="body-1 text-neutrals-2">
            {userInfo.base32Address.substr(0, 15)}
          </div>
          <div className="flex items-center overflow-hidden">
            <span className="caption-bold text-neutrals-4">
              {userInfo.address}
            </span>
            <SVG className="ml-2" src={TokenIcon.src} />
          </div>
          <div className="p-2 mt-2 shadow-depth-2 bg-neutrals-8 rounded-[16px]">
            <div className="flex items-center">
              <SVG className="ml-2 w-[40px] text-gray-600" src={LiskIcon.src} />
              <div className="ml-4">
                <div className="caption-2 text-neutrals-4">Balance</div>
                <div className="body-1 text-neutrals-1 whitespace-nowrap">
                  {data
                    ? transactions.convertBeddowsToLSK(
                        data.data.token.balance + ''
                      )
                    : 0}{' '}
                  LSK
                </div>
              </div>
            </div>
          </div>
          <div className="mt-5">
            <div className="flex items-center text-neutrals-4">
              <SVG src={ProfileIcon.src} />
              <a href="#" className="ml-4 button-2">
                My profile
              </a>
            </div>
            <Divided />
            <div className="flex items-center text-neutrals-4">
              <SVG src={MyItemIcon.src} />
              <Link href="/items" passHref>
                <a className="ml-4 button-2">My items</a>
              </Link>
            </div>
            <Divided />
            <div className="flex items-center text-neutrals-4">
              <SVG src={DarkThemeIcon.src} />
              <a href="#" className="ml-4 button-2">
                Dark theme
              </a>
            </div>
            <Divided />
            <button onClick={disconnect}>
              <div className="flex items-center text-neutrals-4">
                <SVG src={SignOutIcon.src} />
                <a href="#" className="ml-4 button-2">
                  Disconnnect
                </a>
              </div>
            </button>
          </div>
        </Popover.Panel>
      </Transition>
    </Popover>
  )
}
