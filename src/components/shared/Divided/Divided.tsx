import clsx from 'clsx'
import React from 'react'

interface DividedProps {
  className?: string
}

export const Divided: React.FC<DividedProps> = ({ className }) => (
  <div
    className={clsx('h-[1px] bg-neutrals-6 w-full my-[12px]', className)}
  ></div>
)
