import React from 'react'
import clsx from 'clsx'

type ButtonVariant = 'neutral' | 'light'
type ButtonSize = 'xs' | 'sm' | 'md' | 'block'

interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  variant?: ButtonVariant
  size?: ButtonSize
  block?: boolean
}

export const Button: React.FC<ButtonProps> = ({
  variant = 'light',
  size = 'sm',
  block,
  children,
  className,
  onClick,
}) => {
  const paddingClass = React.useMemo(() => {
    switch (size) {
      case 'xs':
        return 'px-3 py-2'
      case 'sm':
        return 'px-4 py-3'
      case 'md':
        return 'px-6 py-4'
    }
  }, [size])

  const fontSizeClass = React.useMemo(() => {
    switch (size) {
      case 'xs':
      case 'sm':
        return 'text-sm leading-[16px]'
      case 'md':
        return 'text-base leading-[16px]'
    }
  }, [size])

  const colorClass = React.useMemo(() => {
    switch (variant) {
      case 'neutral':
        return 'bg-primary-1 hover:bg-primary-1-variant text-neutrals-8'

      case 'light':
        return 'bg-transparent hover:bg-neutrals-2 hover:text-neutrals-8 border-2 border-neutrals-6 hover:border-neutrals-2'
    }
  }, [variant])
  return (
    <button
      className={clsx(
        'transition font-bold text-sm rounded-full focus:outline-none',
        paddingClass,
        block && 'w-full',
        fontSizeClass,
        colorClass,
        className
      )}
      onClick={onClick}
    >
      {children}
    </button>
  )
}
