import { CloseIcon, FilterIcon, LoadingIcon } from '@/assets/icons/icons'
import React from 'react'
import SVG from 'react-inlinesvg'
import { Button } from '../Button/Button'
import { ActivityItem } from './ActivityItem'
import { Filter } from './Filter/Filter'

export const Activity = () => {
  const [showFilter, setShowFilter] = React.useState(false)

  const handleShowFilter = () => {
    setShowFilter(!showFilter)
  }
  return (
    <div className="px-8 mt-8 tablet:px-20 desktop:px-40">
      <div className="flex flex-col-reverse items-start justify-center tablet:flex-row">
        <div className="w-full tablet:w-5/8 desktop:w-2/3 mr-0 tablet:mr-[32px] desktop:mr-[128px]">
          <div className="flex flex-col items-center justify-between tablet:flex-row">
            <div className="flex items-center justify-between w-full">
              <div className="headline-2 text-neutrals-2">Activity</div>
              <div className="inline tablet:hidden">
                {!showFilter ? (
                  <button
                    onClick={handleShowFilter}
                    className="flex items-center justify-center w-10 h-10 transition border-2 border-solid rounded-full focus:outline-none ouline-none border-neutrals-6"
                  >
                    <SVG src={FilterIcon.src} className="w-5 h-5" />
                  </button>
                ) : (
                  <button
                    onClick={handleShowFilter}
                    className="flex items-center justify-center w-10 h-10 p-2 transition border-2 border-solid rounded-full bg-primary-1 focus:outline-none ouline-none border-neutrals-6"
                  >
                    <SVG className="text-neutrals-8" src={CloseIcon.src} />
                  </button>
                )}
              </div>
            </div>
            <div className="block w-full mt-4 tablet:hidden tablet:w-3/8 desktop:w-1/3">
              {showFilter ? <Filter /> : null}
            </div>
            <Button
              variant="light"
              className="w-full mt-4 tablet:w-auto tablet:mt-0 whitespace-nowrap"
            >
              Mark all as read
            </Button>
          </div>
          <div className="flex items-center justify-between mt-8 mb-4 tablet:justify-start desktop:justify-start">
            <div className="bg-neutrals-3 rounded-full py-[6px] px-[12px] button-2 text-neutrals-8">
              My activity
            </div>
            <div className="button-2 text-neutrals-4 mx-[12px] px-[12px]">
              Following
            </div>
            <div className="button-2 text-neutrals-4 px-[12px]">
              All activity
            </div>
          </div>
          <ActivityItem
            title="SomLSKing went wrong"
            content="Can't display activity card. Try again later"
            time="2 days ago"
            type="flag"
            read={false}
          />
          <ActivityItem
            title="UI8"
            content="started following you"
            time="2 days ago"
            type="video"
            read
          />
          <ActivityItem
            title="LSK received"
            content="0.08 LSK received"
            time="2 days ago"
            type="download"
            read={false}
          />
          <ActivityItem
            title="C O I N Z"
            content="purchased by You for O.001 LSK from UI8"
            time="2 days ago"
            type="notify"
            read={false}
          />
          <ActivityItem
            title="LSK received"
            content="0.08 LSK received"
            time="2 days ago"
            type="payment"
            read={false}
          />
          <div className="flex items-center justify-center">
            <SVG src={LoadingIcon.src} className="w-6 h-6" />
          </div>
        </div>
        <div className="hidden w-full tablet:block tablet:w-3/8 desktop:w-1/3">
          <Filter />
        </div>
      </div>
    </div>
  )
}
