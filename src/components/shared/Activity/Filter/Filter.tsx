import { Checkbox } from '@/components/common/Checkbox'
import React from 'react'
import { Button } from '../../Button/Button'

export const Filter = () => {
  return (
    <div className="shadow-depth-3 p-8 border border-solid border-neutrals-6 rounded-[24px]">
      <div className="mb-2 body-1-bold text-neutrals-2">Filters</div>
      <Checkbox title="Sales" checked />
      <Checkbox title="Listings" />
      <Checkbox title="Bids" checked />
      <Checkbox title="Burns" />
      <Checkbox title="Followings" />
      <Checkbox title="Likes" checked />
      <Checkbox title="Purchase" />
      <Checkbox title="Tranfers" checked />
      <div className="my-8 h-[1px] w-full bg-neutrals-6 rounded-[1px]"></div>
      <Button variant="light" block>
        Select all
      </Button>
      <Button variant="light" className="mt-4" block>
        Unselect all
      </Button>
    </div>
  )
}
