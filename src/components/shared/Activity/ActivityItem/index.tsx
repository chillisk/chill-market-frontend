import {
  ArrowRightIcon,
  DownloadIcon,
  FlagIcon,
  NewIcon,
  NextIcon,
  NotifyIcon,
  PaymentIcon,
  VideoIcon,
} from '@/assets/icons/icons'
import clsx from 'clsx'
import React from 'react'
import SVG from 'react-inlinesvg'

type Type =
  | 'flag'
  | 'notification'
  | 'payment'
  | 'download'
  | 'video'
  | 'notify'

interface ActivityItemProps {
  title: string
  content: string
  time: string
  type: Type
  read: boolean
}

export const ActivityItem: React.FC<ActivityItemProps> = ({
  title,
  content,
  time,
  type,
  read,
}) => {
  const renderType = () => {
    switch (type) {
      case 'flag':
        return <SVG src={FlagIcon.src} className="absolute top-0 right-0" />
      case 'video':
        return <SVG src={VideoIcon.src} className="absolute top-0 right-0" />
      case 'download':
        return <SVG src={DownloadIcon.src} className="absolute top-0 right-0" />
      case 'notify':
        return <SVG src={NotifyIcon.src} className="absolute top-0 right-0" />
      case 'payment':
        return <SVG src={PaymentIcon.src} className="absolute top-0 right-0" />
    }
  }
  return (
    <div
      className={clsx(
        'flex justify-between mt-4 pl-4 pr-5 py-4 rounded-[20px]',
        read ? 'bg-neutrals-7 items-center' : 'bg-transparent items-start'
      )}
    >
      <div className="relative">
        <div className="w-16 h-16 bg-red-300 rounded-full"></div>
        {renderType()}
      </div>
      <div className="float-left w-full mx-6 font-poppins">
        <div className="body-2-bold tablet:body-1-bold text-neutrals-1">
          {title}
        </div>
        <div className="caption-2 tablet:body-2 text-neutrals-3">{content}</div>
        <div className="caption-2-bold text-neutrals-4">{time}</div>
      </div>
      {read ? (
        <SVG src={NextIcon.src} className="w-4 h-4" />
      ) : (
        <SVG src={NewIcon.src} className="w-5 h-5" />
      )}
    </div>
  )
}
