import { nodeInfoAtom } from '@/atom/node-info'
import { getAllOnSaleNFT } from '@/components/Home/Feed/FeaturedItem/FeaturedItem'
import { sendTransactions } from '@/services/api'
import {
  PurchaseNFTParams,
  purchaseNFTToken,
} from '@/utils/transactions/purchase-nft-token'
import {
  Button,
  FormControl,
  FormLabel,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useToast,
} from '@chakra-ui/react'
import { useAtom } from 'jotai'
import React from 'react'
import { useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'

interface PurchaseModalProps {
  nftId: string
  name: string
  isOpen: boolean
  onClose: () => void
  onOpen: () => void
}

export const PurchaseModal: React.FC<PurchaseModalProps> = ({
  nftId,
  isOpen,
  onClose,
  onOpen,
  name,
}) => {
  const { register, handleSubmit, reset } = useForm()
  const [nodeInfo] = useAtom(nodeInfoAtom)
  const client = useQueryClient()
  const toast = useToast()
  const { mutate, isLoading } = useMutation(
    async (e: any) => {
      const data: PurchaseNFTParams = {
        passphrase: e.passphrase,
        purchaseValue: e.purchaseValue,
        nftId,
        minFeePerByte: nodeInfo.minFeePerByte,
        networkIdentifier: nodeInfo.networkIdentifier,
      }
      const res = await purchaseNFTToken(data)
      const saleResult = await sendTransactions(res.tx)
      return saleResult
    },
    {
      onSuccess() {
        onClose()
        reset()
        toast({
          title: `You are owner of ${name}.`,
          status: 'success',
          duration: 9000,
          isClosable: true,
        })
        client.invalidateQueries(getAllOnSaleNFT)
      },
      onError() {
        onClose()
        reset()
        toast({
          title: "Can't buy this NFT",
          status: 'error',
          duration: 9000,
          isClosable: true,
        })
      },
    }
  )
  const onConfirm = (d: any) => {
    mutate(d)
  }
  return (
    <Modal isOpen={isOpen} onClose={onClose} isCentered>
      <form onSubmit={handleSubmit(onConfirm)}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>
            Buy <span className="text-primary-1">{name}</span>
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <FormControl id="purchaseValue" isRequired>
              <FormLabel>Purchase Value</FormLabel>
              <Input
                placeholder="Enter your purchase value"
                {...register('purchaseValue', { required: true })}
              />
            </FormControl>
            <FormControl id="passphrase" isRequired className="mt-4">
              <FormLabel>Passphrase</FormLabel>
              <Input
                placeholder="Enter your passphrase"
                {...register('passphrase', { required: true })}
              />
            </FormControl>
          </ModalBody>

          <ModalFooter>
            <Button variant="ghost" mr={3} onClick={onClose}>
              Cancel
            </Button>
            <Button colorScheme="blue" type="submit">
              Confirm
            </Button>
          </ModalFooter>
        </ModalContent>
      </form>
    </Modal>
  )
}
