import React from 'react'

export const RangeSlider = () => {
  return (
    <div className="flex items-center justify-center w-full h-16 m-auto">
      <div className="relative min-w-full py-1">
        <div className="h-2 bg-gray-200 rounded-full">
          <div
            className="absolute w-0 h-2 rounded-full bg-primary-1"
            style={{ width: '58.5714%' }}
          ></div>
          <div
            className="absolute top-0 flex items-center justify-center w-4 h-4 -ml-2 bg-white border border-gray-300 rounded-full shadow cursor-pointer"
            unselectable="on"
            style={{ left: '58.5714% ' }}
          >
            <div className="relative w-1 -mt-2">
              <div
                className="absolute left-0 z-40 min-w-full mb-2 opacity-100 bottom-100"
                style={{ marginLeft: '-20.5px' }}
              >
                <div className="relative shadow-md">
                  <div className="px-4 py-1 -mt-8 text-xs text-white truncate rounded bg-neutrals-1">
                    6
                  </div>
                  <svg
                    className="absolute left-0 w-full h-2 text-black top-100"
                    x="0px"
                    y="0px"
                    viewBox="0 0 255 255"
                    xmlSpace="preserve"
                  >
                    <polygon
                      className="fill-current"
                      points="0,0 127.5,127.5 255,0"
                    ></polygon>
                  </svg>
                </div>
              </div>
            </div>
          </div>
          <div className="absolute bottom-0 left-0 -mb-6 -ml-1 caption-bold">
            0.01 LSK
          </div>
          <div className="absolute bottom-0 right-0 -mb-6 -mr-1 caption-bold">
            10 LSK
          </div>
        </div>
      </div>
    </div>
  )
}
