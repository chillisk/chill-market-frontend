import { OwnerIcon } from '@/assets/icons/icons'
import React from 'react'
import SVG from 'react-inlinesvg'
import { Button } from '../Button/Button'
import { Divided } from '../Divided/Divided'
import { Interactive } from './Interactive/Interactive'

export const ItemDetail = () => {
  return (
    <div className="relative flex flex-col px-8 mt-8 tablet:px-20 desktop:px-40 tablet:flex-row">
      <div className="relative w-full h-[420px] bg-blue-300 rounded-[16px] mr-0 tablet:mr-[32px] desktop:mr-[96px] tablet:w-5/8 desktop:w-3/4">
        <div className="absolute flex items-center mt-5 top-5 left-5">
          <div className="w-auto p-2 uppercase hairline-2 text-neutrals-8 bg-neutrals-2 rounded-[4px]">
            Art
          </div>
          <div className="w-auto p-2 ml-2 uppercase hairline-2 text-neutrals-8 bg-primary-2 rounded-[4px]">
            Unlockable
          </div>
        </div>
        <div className="absolute flex items-center justify-center w-full desktop:hidden bottom-8">
          <Interactive />
        </div>
      </div>
      <div className="w-full right-comp tablet:w-3/8 desktop:w-1/4 tablet:mt-0 mt-[32px] ">
        <div className="tracking-tight headline-3 text-neutrals-2">
          The amazing art
        </div>
        <div className="flex items-center mt-2">
          <div className="p-2 border-2 border-solid border-primary-4 button-1 text-primary-4 rounded-[4px]">
            2.5 LSK
          </div>
          <div className="p-2 ml-2 border-2 border-solid border-neutrals-6 button-1 text-neutrals-4 rounded-[4px] price">
            $4,429.87
          </div>
          <div className="p-2 ml-2 button-1 text-neutrals-4">10 in stock</div>
        </div>
        <div className="mt-[40px]">
          <div className="body-2 text-neutrals-4">
            This NFT Card will give you Access to Special Airdrops. To learn
            more about UI8 please visit{' '}
            <span className="text-neutrals-2">https://ui8.net</span>
          </div>
        </div>
        <div className="mt-[40px]">
          <div className="flex items-center justify-between mt-8 mb-4 tablet:justify-start desktop:justify-start p-[6px] border-2 border-solid border-neutrals-6 rounded-[36px]">
            <div className="bg-neutrals-3 rounded-full py-[6px] px-[12px] button-2 text-neutrals-8">
              Info
            </div>
            <div className="button-2 text-neutrals-4 ml-[12px] px-[12px]">
              Owners
            </div>
            <div className="button-2 text-neutrals-4 ml-[12px] px-[12px]">
              History
            </div>
            <div className="button-2 text-neutrals-4 ml-[12px] px-[12px]">
              Bids
            </div>
          </div>
          <div>
            <div className="flex items-center">
              <div className="relative w-12 h-12 bg-blue-300 rounded-full">
                <SVG
                  src={OwnerIcon.src}
                  className="absolute bottom-0 right-0"
                />
              </div>
              <div className="ml-4">
                <div className="caption text-neutrals-4">Owner</div>
                <div className="caption-bold text-neutrals-2 mt-[2px]">
                  Raquel Will
                </div>
              </div>
            </div>
            <Divided />
            <div className="flex items-center">
              <div className="relative w-12 h-12 bg-blue-300 rounded-full"></div>
              <div className="ml-4">
                <div className="caption text-neutrals-4">Creator</div>
                <div className="caption-bold text-neutrals-2 mt-[2px]">
                  Raquel Will
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="mt-[48px] rounded-[16px] shadow-lg p-4 border border-solid border-neutrals-6">
          <div className="flex items-center">
            <div className="relative w-12 h-12 bg-blue-300 rounded-full"></div>
            <div className="ml-4">
              <div className="body-2-bold">
                <span className="text-neutrals-4">Highest bid by</span>
                <span className="ml-2 text-neutrals-2">Kohaku Tora</span>
              </div>
              <div className="body-1-bold text-neutrals-2 mt-[2px]">
                <span className="text-neutrals-2">1.46 LSK</span>
                <span className="ml-[12px] text-neutrals-4">$2,764.89</span>
              </div>
            </div>
          </div>
          <div className="my-[32px] flex items-center">
            <Button className="mr-2 text-white" variant="neutral" block>
              Purchase now
            </Button>
            <Button className="ml-2" block>
              Place a bid
            </Button>
          </div>
          <div className="flex items-center">
            <span className="caption-bold text-neutrals-4">Service free</span>
            <span className="caption-bold ml-[12px] text-neutrals-2">1.5%</span>
            <span className="caption ml-[12px] text-neutrals-4">2.563 LSK</span>
            <span className="caption ml-[12px] text-neutrals-4">$4,540.62</span>
          </div>
        </div>
      </div>
      <div className="absolute hidden desktop:block right-[56px]">
        <Interactive />
      </div>
    </div>
  )
}
