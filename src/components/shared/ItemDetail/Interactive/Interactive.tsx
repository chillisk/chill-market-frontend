import {
  CloseIcon,
  HeartIcon,
  MoreIcon,
  UploadIcon,
} from '@/assets/icons/icons'
import React from 'react'
import SVG from 'react-inlinesvg'

export const Interactive = () => {
  return (
    <div className="flex items-center self-center justify-between shadow-lg w-auto p-2 bg-white rounded-[64px] gap-5 desktop:bg-transparent desktop:shadow-none desktop:flex-col">
      <div className="items-center justify-center hidden w-10 h-10 p-2 transition border-2 rounded-full cursor-pointer bg-neutrals-2 desktop:flex border-neutrals-6 hover:bg-neutrals-7">
        <SVG src={CloseIcon.src} className="text-neutrals-4" />
      </div>
      <div className="flex items-center justify-center w-10 h-10 transition border-2 rounded-full cursor-pointer border-neutrals-6 hover:bg-neutrals-7">
        <SVG src={UploadIcon.src} className="text-neutrals-4" />
      </div>
      <div className="flex items-center justify-center w-10 h-10 transition border-2 rounded-full cursor-pointer border-neutrals-6 hover:bg-neutrals-7">
        <SVG src={HeartIcon.src} className="text-primary-3" />
      </div>
      <div className="flex items-center justify-center w-10 h-10 transition border-2 rounded-full cursor-pointer border-neutrals-6 hover:bg-neutrals-7">
        <SVG src={MoreIcon.src} className="text-neutrals-4" />
      </div>
    </div>
  )
}
