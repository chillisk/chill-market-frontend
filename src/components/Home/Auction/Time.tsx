import React from 'react'

interface TimeProps {
  value: number
  label: string
}

export const Time: React.FC<TimeProps> = ({ value, label }) => {
  return (
    <div className="w-16">
      <div className="headline-4 text-neutrals-1">{value}</div>
      <div className="body-2-bold text-neutrals-4">{label}</div>
    </div>
  )
}
