import React from 'react'

interface CreatorPriceProps {
  name: string
  desc: string
}

export const CreatorPrice: React.FC<CreatorPriceProps> = ({
  children,
  name,
  desc,
}) => {
  return (
    <div className="flex gap-2">
      <div className="w-10 h-10 overflow-hidden rounded-full">{children}</div>
      <div>
        <div className="caption-2 text-neutrals-4">{name}</div>
        <div className="caption-bold text-neutrals-2">{desc}</div>
      </div>
    </div>
  )
}
