import React from 'react'
import Image from 'next/image'
import SVG from 'react-inlinesvg'
import { CreatorPrice } from './Creator'

import videoMockupImg from '@/assets/images/video-mockup.png'
import creatorImage from '@/assets/images/users/01.png'
import { StopIcon, ArrowLeftIcon, ArrowRightIcon } from '@/assets/icons/icons'
import { Time } from './Time'
import { Button } from '@/components/shared/Button/Button'

export const Auction = () => {
  return (
    <div className="block gap-8 pt-8 tablet:pt-16 tablet:grid-cols-8 desktop:grid-cols-12 tablet:grid">
      <div className="w-full desktop:col-span-7 tablet:col-span-5">
        <Image src={videoMockupImg} alt="Auction video" layout="responsive" />
      </div>
      <div className="w-full mt-8 tablet:mt-0 tablet:col-span-3 desktop:col-span-4 desktop:col-start-9">
        <h1 className="headline-1 text-neutrals-2">the creator network&reg;</h1>
        <div className="grid grid-cols-2 gap-8 mt-5">
          <CreatorPrice name="Creator" desc="Enrico Cole">
            <Image src={creatorImage} alt="creator" />
          </CreatorPrice>
          <CreatorPrice name="Instant price" desc="3.5 LSK">
            <div className="flex items-center justify-center w-10 h-10 text-neutrals-8 bg-primary-4">
              <SVG src={StopIcon.src} />
            </div>
          </CreatorPrice>
        </div>
        <div className="p-8 mt-10 text-center border-2 bg-neutrals-8 shadow-depth-4 border-neutrals-6 rounded-3xl">
          <div className="body-2-bold text-neutrals-2">Current Bid</div>
          <h2 className="price headline-2">1.00 LSK</h2>
          <p className="price-usd body-1-bold text-neutrals-4">$3,618.36</p>
          <div className="flex flex-col items-center mt-6 auction-time">
            <div className="text-neutrals-2 body-2-bold">Auction ending in</div>
            <div className="flex gap-[19.5px] mt-2">
              <Time value={19} label="Hrs" />
              <Time value={24} label="mins" />
              <Time value={19} label="secs" />
            </div>
          </div>
        </div>

        <div className="mt-10 actions">
          <Button className="w-full" variant="neutral">
            Place a bid
          </Button>
          <Button className="w-full mt-2">View item</Button>
        </div>
        <div className="flex justify-center mt-10 tablet:justify-start controls text-neutrals-4">
          <div className="flex items-center justify-center w-10 h-10 transition border-2 border-transparent rounded-full cursor-pointer hover:border-neutrals-6">
            <SVG src={ArrowLeftIcon.src} />
          </div>
          <div className="flex items-center justify-center w-10 h-10 ml-2 transition border-2 rounded-full cursor-pointer border-neutrals-6 hover:bg-neutrals-7">
            <SVG src={ArrowRightIcon.src} />
          </div>
        </div>
      </div>
    </div>
  )
}
