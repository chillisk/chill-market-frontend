import { Button } from '@/components/shared/Button/Button'
import React from 'react'

export const ContentBlock = () => {
  return (
    <div className="pt-16 pb-8 text-center tablet:pt-32 tablet:pb-16">
      <h4 className="hairline-2 text-neutrals-4">
        Create, explore, & collect digital art NFTs.
      </h4>
      <h3 className="mt-2 headline-4 tablet:headline-3">
        The new creative economy.
      </h3>
      <Button className="mt-6">Start your search</Button>
    </div>
  )
}
