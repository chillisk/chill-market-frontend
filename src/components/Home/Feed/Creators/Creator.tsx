import React from 'react'
import Image from 'next/image'

interface CreatorProps {
  name: string
  number: number
  asset: number
  avatar: string
}
export const Creator: React.FC<CreatorProps> = ({
  name,
  number,
  asset,
  avatar,
}) => {
  return (
    <div className="flex flex-shrink-0 mx-8 border-b desktop:py-6 border-neutrals-6 last:border-0">
      <div className="relative left w-14 h-14">
        <div className="absolute w-6 h-6 text-center border-2 rounded-full -top-1 -left-1 number bg-neutrals-2 border-neutrals-8 caption-2-bold text-neutrals-8">
          {number}
        </div>
        <img src={avatar} alt={name} className="w-14 h-14" />
      </div>
      <div className="flex-1 ml-4 right">
        <div className="caption-bold text-neutrals-2">{name}</div>
        <div className="price">
          <span className="caption-2-bold text-neutrals-3">{asset}</span>
          <span className="ml-0.5 caption-2 text-neutrals-4">LSK</span>
        </div>
      </div>
    </div>
  )
}
