import React from 'react'
import SVG from 'react-inlinesvg'
import user01 from '@/assets/images/users/01-56.png'
import user02 from '@/assets/images/users/02-56.png'
import { Creator } from './Creator'
import { Button } from '@/components/shared/Button/Button'
import { ArrowRightIcon } from '@/assets/icons/icons'

export const Creators = () => {
  return (
    <div className="col-span-8 mt-10 desktop:col-span-3 tablet:mt-0">
      <div className="caption-2-bold text-neutrals-4">
        Latest upload from creators 🔥
      </div>
      <div className="flex items-center pt-4 -mx-8 overflow-x-auto desktop:block desktop:overflow-auto">
        <Creator
          name="Payton Harris"
          asset={2.456}
          avatar={user01.src}
          number={6}
        />
        <Creator
          name="Anita Bins"
          asset={2.456}
          avatar={user02.src}
          number={6}
        />
        <Creator
          name="Payton Harris"
          asset={2.456}
          avatar={user01.src}
          number={6}
        />
        <Creator
          name="Anita Bins"
          asset={2.456}
          avatar={user02.src}
          number={6}
        />
      </div>
      <Button className="items-center hidden desktop:flex">
        <div>Discover more</div>
        <SVG src={ArrowRightIcon.src} className="w-4 h-4 ml-3" />
      </Button>
    </div>
  )
}
