import { Button, useDisclosure } from '@chakra-ui/react'
import React from 'react'
import Avatar from 'boring-avatars'
import { Price } from './Price'
import { PurchaseModal } from '@/components/shared/Purchase/PurchaseModal'
interface SmallFeaturedItemProps {
  nftId: string
  src: string
  name: string
  authorAddress: string
  price: number
  currency: string
}

export const SmallFeaturedItem: React.FC<SmallFeaturedItemProps> = ({
  nftId,
  src,
  name,
  authorAddress = '',
  price,
  currency,
}) => {
  const { isOpen, onOpen, onClose } = useDisclosure()
  return (
    <div className="flex items-center gap-6 mb-8">
      <img
        src={src}
        alt="name"
        className="object-cover w-40 h-36 rounded-2xl"
      />
      <div className="flex-1">
        <div className="body-2-bold text-neutrals-2">{name}</div>
        <div className="flex items-center gap-1 mt-2">
          <Avatar
            size={24}
            name={authorAddress}
            variant="marble"
            colors={['#92A1C6', '#146A7C', '#F0AB3D', '#C271B4', '#C20D90']}
          />
          <div className="mr-auto">{authorAddress.substr(0, 10)}...</div>
          <Price value={price} currency={currency} />
        </div>
        <Button className="mt-4" size="xs" rounded="full" onClick={onOpen}>
          Buy now
        </Button>
      </div>
      <PurchaseModal
        {...{
          isOpen,
          onClose,
          onOpen,
          nftId,
          name,
        }}
      />
    </div>
  )
}
