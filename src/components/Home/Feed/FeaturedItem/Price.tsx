import React from 'react'

interface PriceProps {
  value: number
  currency: string
}

export const Price: React.FC<PriceProps> = ({ value, currency }) => {
  return (
    <div className="p-2 pb-[6px] border-2 rounded hairline-2 text-primary-4 border-primary-4">
      {value} {currency}
    </div>
  )
}
