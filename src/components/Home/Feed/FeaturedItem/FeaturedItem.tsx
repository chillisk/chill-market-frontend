import React from 'react'
import Image from 'next/image'
import featureImg from '@/assets/images/items/01.png'
import userImg from '@/assets/images/users/01-48.png'
import { SmallFeaturedItem } from './SmallFeaturedItem'
import get from 'lodash.get'
import itemImg1 from '@/assets/images/items/02.png'
import Avatar from 'boring-avatars'
import { PurchaseModal } from '@/components/shared/Purchase/PurchaseModal'
import user1 from '@/assets/images/users/01.png'
import { Price } from './Price'
import { Button, Spinner, useDisclosure } from '@chakra-ui/react'
import { useQuery } from 'react-query'
import { getAllOnSaleTokens, ListNFT, NFT } from '@/services/api/nft'
import { transactions } from '@liskhq/lisk-client'

export const getAllOnSaleNFT = 'get-all-onsale-nft'
export const FeaturedItem = () => {
  const { data, isLoading } = useQuery(getAllOnSaleNFT, () =>
    getAllOnSaleTokens()
  )
  const one: NFT = get(data, 'data.[0]')
  const two: NFT = get(data, 'data.[1]')
  const three: NFT = get(data, 'data.[2]')
  const four: NFT = get(data, 'data.[3]')
  const five: NFT = get(data, 'data.[4]')
  const six: NFT = get(data, 'data.[5]')
  const seven: NFT = get(data, 'data.[6]')
  const { isOpen, onOpen, onClose } = useDisclosure()
  return (
    <>
      {isLoading ? (
        <div className="flex items-center justify-center w-full h-96">
          <Spinner />
        </div>
      ) : (
        <React.Fragment>
          {one ? (
            <div className="featured-item tablet:col-span-4 desktop:col-span-4">
              <div className="overflow-hidden main-img rounded-xl">
                <img
                  src={one.url}
                  alt={one.name}
                  className="object-cover w-full max-h-[400px]"
                />
              </div>
              <div className="flex gap-4 mt-6 info">
                <div className="w-12 h-12 rounded-full">
                  <Avatar
                    size={48}
                    name={one.ownerAddress}
                    variant="marble"
                    colors={[
                      '#92A1C6',
                      '#146A7C',
                      '#F0AB3D',
                      '#C271B4',
                      '#C20D90',
                    ]}
                  />
                </div>
                <div className="flex justify-between w-full right">
                  <div className="desc">
                    <div className="body-2-bold text-neutrals-2">
                      {one.name}
                    </div>
                    <div className="caption-2-bold text-neutrals-3">
                      {one.ownerAddress.substr(0, 20)}...
                    </div>
                  </div>
                  <div className="text-right price">
                    <Price
                      value={+transactions.convertBeddowsToLSK(one.value)}
                      currency="LSK"
                    />
                    <Button
                      size="sm"
                      rounded="full"
                      className="mt-2"
                      onClick={onOpen}
                    >
                      Buy now
                    </Button>
                  </div>
                </div>
              </div>
              <PurchaseModal
                {...{
                  isOpen,
                  onClose,
                  onOpen,
                  nftId: one.id,
                  name: one.name,
                }}
              />
            </div>
          ) : null}
          <div className="hidden right desktop:block desktop:col-span-4">
            {two ? (
              <SmallFeaturedItem
                nftId={two.id}
                src={two.url}
                name={two.name}
                price={+transactions.convertBeddowsToLSK(two.value)}
                currency="LSK"
                authorAddress={two.ownerAddress}
              />
            ) : null}
            {three ? (
              <SmallFeaturedItem
                nftId={three.id}
                src={three.url}
                name={three.name}
                price={+transactions.convertBeddowsToLSK(three.value)}
                currency="LSK"
                authorAddress={three.ownerAddress}
              />
            ) : null}
            {four ? (
              <SmallFeaturedItem
                nftId={four.id}
                src={four.url}
                name={four.name}
                price={+transactions.convertBeddowsToLSK(four.value)}
                currency="LSK"
                authorAddress={four.ownerAddress}
              />
            ) : null}
          </div>
          <div className="hidden right tablet:block tablet:col-span-4">
            {five ? (
              <SmallFeaturedItem
                nftId={five.id}
                src={five.url}
                name={five.name}
                price={+transactions.convertBeddowsToLSK(five.value)}
                currency="LSK"
                authorAddress={five.ownerAddress}
              />
            ) : null}
            {six ? (
              <SmallFeaturedItem
                nftId={six.id}
                src={six.url}
                name={six.name}
                price={+transactions.convertBeddowsToLSK(six.value)}
                currency="LSK"
                authorAddress={six.ownerAddress}
              />
            ) : null}
            {seven ? (
              <SmallFeaturedItem
                nftId={seven.id}
                src={seven.url}
                name={seven.name}
                price={+transactions.convertBeddowsToLSK(seven.value)}
                currency="LSK"
                authorAddress={seven.ownerAddress}
              />
            ) : null}
          </div>
        </React.Fragment>
      )}
    </>
  )
}
