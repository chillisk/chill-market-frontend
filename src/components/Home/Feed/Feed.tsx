import React from 'react'
import { Creators } from './Creators/Creators'
import { FeaturedItem } from './FeaturedItem/FeaturedItem'

export const Feed = () => {
  return (
    <div className="block pt-16 tablet:grid tablet:grid-cols-8 tablet:gap-8 desktop:grid-cols-12">
      <FeaturedItem />
      {/* <div className="block tablet:hidden w-full h-[1px] bg-neutrals-6 mt-10"></div>
      <Creators /> */}
    </div>
  )
}
