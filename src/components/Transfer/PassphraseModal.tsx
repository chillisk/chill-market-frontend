import React from 'react'
import { FormControl, FormLabel, Input, Button, useDisclosure, Modal, ModalOverlay, ModalContent, ModalHeader, ModalCloseButton, ModalBody, ModalFooter } from '@chakra-ui/react'
import { useForm } from 'react-hook-form'

interface PassphraseModalProps {
  isOpen: boolean
  onClose: () => void
  onSubmit: (passphrase: string) => void
}

export const PassphraseModal: React.FC<PassphraseModalProps> = ({ isOpen, onClose, onSubmit }) => {
  const { register, handleSubmit } = useForm()
  const onConfirm = (d: any) => {
    onClose()
    onSubmit(d.passphrase)
  }
  return (
    <Modal isOpen={isOpen} onClose={onClose} isCentered>
      <form onSubmit={handleSubmit(onConfirm)}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Confirm Transaction</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <FormControl id="passphrase" isRequired>
              <FormLabel>Passphrase</FormLabel>
              <Input placeholder="Enter your passphrase" {...register('passphrase', { required: true })} />
            </FormControl>
          </ModalBody>

          <ModalFooter>
            <Button variant="ghost" mr={3} onClick={onClose}>Cancel</Button>
            <Button colorScheme="blue" type="submit">
              Confirm
            </Button>
          </ModalFooter>
        </ModalContent>
      </form>
    </Modal>
  )
}
