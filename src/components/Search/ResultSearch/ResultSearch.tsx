import { Divided } from '@/components/shared/Divided/Divided'
import React from 'react'

interface ResultSearchProps {
  title: string
  value: number
  amountStock: number
  bidHighest: number
  type: 'NEW'
}

export const ResultSearch: React.FC<ResultSearchProps> = ({
  title,
  value,
  amountStock,
  bidHighest,
  type,
}) => {
  return (
    <div className="p-[12px] shadow-depth-4 rounded-[20px] m-[16px] w-full md:w-auto">
      <div className="w-full h-[303px] bg-blue-300 rounded-[16px]" />
      <div className="mt-[20px]">
        <div className="flex items-center justify-between">
          <div className="mr-2 body-2-bold text-neutrals-2 whitespace-nowrap">
            {title}
          </div>
          <div className="hairline-2 text-primary-4 p-2 border-2 border-solid border-primary-4 rounded-[4px]">
            {value} LSK
          </div>
        </div>
        <div className="mt-[12px] flex items-center justify-between">
          <div className="flex">
            {Array.from([1, 2, 3]).map((item) => {
              return (
                <div key={item}>
                  <div className="flex items-center justify-center w-5 h-5">
                    <div className="w-5 h-5 bg-red-300 rounded-full "></div>
                  </div>
                </div>
              )
            })}
          </div>
          <div className="caption-bold text-neutrals-3">
            {amountStock} in stock
          </div>
        </div>
        <Divided />
        <div className="flex items-center justify-between mb-[20px]">
          <div className="caption-2">
            Highest bid <span className="caption-2-bold">{bidHighest} LSK</span>
          </div>
          {type === 'NEW' ? <div className="caption-2">New bid 🔥</div> : null}
        </div>
      </div>
    </div>
  )
}
