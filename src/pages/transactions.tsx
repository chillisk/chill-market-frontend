import { Header } from '@/components/shared/Header/Header'
import { TransferContainer } from '@/containers/Transfer/Transfer'
import { getAllTransactions } from '@/services/api'
import React from 'react'
import { useQuery } from 'react-query'
import {
  Table,
  TableCaption,
  Tbody,
  Td,
  Tfoot,
  Th,
  Thead,
  Tr,
} from '@chakra-ui/react'
import { cryptography } from '@liskhq/lisk-client'

const columns = [
  { id: 'moduleName', label: 'Module Name', minWidth: 100, maxWidth: 50 },
  { id: 'assetName', label: 'Asset Name', minWidth: 100, maxWidth: 50 },
  { id: 'address', label: 'Address', minWidth: 170, maxWidth: 50 },
  { id: 'id', label: 'Transaction ID', minWidth: 170, maxWidth: 50 },
]

const TransactionsPage = () => {
  const { data, isLoading } = useQuery('get-all-transation', () =>
    getAllTransactions()
  )
  return (
    <React.Fragment>
      <Header />
      <div className="px-8 tablet:px-20 desktop:px-40">
        <div className="p-4 mt-4 bg-white rounded shadow">
          <Table variant="striped">
            <Thead>
              <Tr>
                {columns.map((column) => (
                  <Th key={column.id} style={{ minWidth: column.minWidth }}>
                    {column.label}
                  </Th>
                ))}
              </Tr>
            </Thead>
            <Tbody>
              {isLoading
                ? null
                : data.data.map((row) => {
                    return (
                      <Tr key={row.id}>
                        {columns.map((column, id) => {
                          let base32UIAddress
                          let value
                          if (row['recipientAddress']) {
                            base32UIAddress =
                              cryptography.getBase32AddressFromAddress(
                                Buffer.from(row['recipientAddress'], 'hex')
                              )
                            value = base32UIAddress
                          } else if (row['senderPublicKey']) {
                            base32UIAddress = cryptography
                              .getBase32AddressFromPublicKey(
                                Buffer.from(row['senderPublicKey'], 'hex'),
                                'lsk'
                              )
                              .toString()
                            value = base32UIAddress
                          }
                          value = row[column.id]
                          return (
                            <Td key={column.id}>
                              {column.id === 'address'
                                ? base32UIAddress
                                : value}
                            </Td>
                          )
                        })}
                      </Tr>
                    )
                  })}
            </Tbody>
          </Table>
        </div>
      </div>
    </React.Fragment>
  )
}

export default TransactionsPage
