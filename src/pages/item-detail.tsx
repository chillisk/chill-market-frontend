import { Header } from '@/components/shared/Header/Header'
import { ItemDetail } from '@/components/shared/ItemDetail/ItemDetail'
import React from 'react'

const ActivityPage = () => {
  return (
    <>
      <Header />
      <ItemDetail />
    </>
  )
}

export default ActivityPage
