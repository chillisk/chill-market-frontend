import { ResultSearch } from '@/components/Search/ResultSearch/ResultSearch'
import { Divided } from '@/components/shared/Divided/Divided'
import { Header } from '@/components/shared/Header/Header'
import { Search } from '@/components/shared/Header/Search/Search'
import { Select } from '@/components/shared/Header/Select/Select'
import SVG from 'react-inlinesvg'
import React from 'react'
import { LoadingIcon, ResetFilterIcon } from '@/assets/icons/icons'
import { RangeSlider } from '@/components/shared/RangeSlider/RangeSlider'
import { Button } from '@/components/shared/Button/Button'

const SearchPage = () => {
  return (
    <React.Fragment>
      <Header />
      <div className="flex flex-col items-start w-full px-8 tablet:px-20 desktop:px-40 tablet:flex-row">
        <div className="w-full mr-0 tablet:w-3/8 desktop:w-1/4 tablet:mr-4">
          <Search className="w-full mt-6" placeholder="Type your keywords" />
          <Select className="w-full mt-6" value="Recently added"></Select>
          <div className="flex items-center justify-between mt-8 mb-4 tablet:justify-start desktop:justify-start">
            <div className="bg-neutrals-3 rounded-full py-[6px] px-[12px] button-2 text-neutrals-8">
              All items
            </div>
            <div className="button-2 text-neutrals-4 mx-[12px] px-[12px]">
              Art
            </div>
            <div className="button-2 text-neutrals-4 px-[12px]">Game</div>
            <div className="button-2 text-neutrals-4 px-[12px]">
              Photography
            </div>
          </div>
          <RangeSlider />
          <Divided className="my-[24px]" />
          <Select className="w-full mt-6" title="likes" value="Most liked" />
          <Select className="w-full mt-6" title="open" value="Colors" />
          <div className="p-4 shadow-depth-4 my-[12px] rounded-[12px]">
            <div className="flex items-center">
              <div className="w-4 h-4 border border-solid rounded-full border-primary-1" />
              <div className="ml-2">All colors</div>
            </div>
            <div className="flex items-center mt-[10px]">
              <div className="w-4 h-4 bg-black border border-solid rounded-full" />
              <div className="ml-2">Black</div>
            </div>
            <div className="flex items-center mt-[10px]">
              <div className="w-4 h-4 bg-green-500 border border-solid rounded-full" />
              <div className="ml-2">Green</div>
            </div>
            <div className="flex items-center mt-[10px]">
              <div className="w-4 h-4 bg-red-500 border border-solid rounded-full" />
              <div className="ml-2">Pink</div>
            </div>
            <div className="flex items-center mt-[10px]">
              <div className="w-4 h-4 bg-purple-500 border border-solid rounded-full" />
              <div className="ml-2">Purple</div>
            </div>
          </div>
          <Select
            className="w-full mt-6"
            title="creator"
            value="Verified only"
          ></Select>
          <Divided className="my-[24px]" />
          <div className="flex items-center">
            <SVG src={ResetFilterIcon.src} />
            <span className="ml-2 button-2 text-neutrals-2">Reset Filter</span>
          </div>
        </div>
        <div className="w-full tablet:w-5/8 desktop:w-3/4">
          <div className="flex flex-wrap">
            <ResultSearch
              title="Amazing digital art"
              value={2.45}
              amountStock={3}
              bidHighest={0.001}
              type="NEW"
            />
            <ResultSearch
              title="Amazing digital art"
              value={2.45}
              amountStock={3}
              bidHighest={0.001}
              type="NEW"
            />
            <ResultSearch
              title="Amazing digital art"
              value={2.45}
              amountStock={3}
              bidHighest={0.001}
              type="NEW"
            />
            <ResultSearch
              title="Amazing digital art"
              value={2.45}
              amountStock={3}
              bidHighest={0.001}
              type="NEW"
            />
            <ResultSearch
              title="Amazing digital art"
              value={2.45}
              amountStock={3}
              bidHighest={0.001}
              type="NEW"
            />
            <ResultSearch
              title="Amazing digital art"
              value={2.45}
              amountStock={3}
              bidHighest={0.001}
              type="NEW"
            />
            <ResultSearch
              title="Amazing digital art"
              value={2.45}
              amountStock={3}
              bidHighest={0.001}
              type="NEW"
            />
            <ResultSearch
              title="Amazing digital art"
              value={2.45}
              amountStock={3}
              bidHighest={0.001}
              type="NEW"
            />
          </div>
          <div className="flex items-center justify-center w-full">
            <Button className="flex items-center justify-center w-full mx-4 md:mx-0 md:w-auto">
              <SVG src={LoadingIcon.src} className="w-4 h-4 mr-3" />
              <div>Load more</div>
            </Button>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

export default SearchPage
