import { Header } from '@/components/shared/Header/Header'
import { HomeContainer } from '@/containers/Home/Home'
import React from 'react'

const IndexPage = () => {
  return (
    <React.Fragment>
      <Header />
      <HomeContainer />
    </React.Fragment>
  )
}

export default IndexPage
