import React, { useEffect } from 'react'
import Head from 'next/head'
import { ChakraProvider } from '@chakra-ui/react'
import { QueryClient, QueryClientProvider } from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'
import 'tailwindcss/tailwind.css'
import { NodeInfo } from '@/containers/NodeInfo'
import { extendTheme, withDefaultColorScheme } from '@chakra-ui/react'

const queryClient = new QueryClient()

// 2. Call `extendTheme` and pass your custom values
const theme = extendTheme(
  {},
  withDefaultColorScheme({
    colorScheme: 'messenger',
  })
)

function MyApp({ Component, pageProps }) {
  return (
    <ChakraProvider theme={theme}>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <QueryClientProvider client={queryClient}>
        <NodeInfo>
          <Component {...pageProps} />
        </NodeInfo>
        <ReactQueryDevtools initialIsOpen={false} />
      </QueryClientProvider>
    </ChakraProvider>
  )
}

export default MyApp
