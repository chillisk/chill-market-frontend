import { userInfoAtom } from '@/atom/address'
import { Header } from '@/components/shared/Header/Header'
import { useAtom } from 'jotai'
import Image from 'next/image'
import ctaImage from '@/assets/images/cta.png'
import React from 'react'
import { Button } from '@chakra-ui/button'
import Link from 'next/link'
import { useQuery } from 'react-query'
import { getAllUserTokens } from '@/services/api/nft'
import CardItem from '@/components/shared/CardItem/CardItem'

export const GetAllUserTokens = 'get-all-user-token'
const ItemsPage = () => {
  const [userInfo] = useAtom(userInfoAtom)
  const { data, isLoading } = useQuery(
    [GetAllUserTokens, userInfo.address],
    () => getAllUserTokens(userInfo.address),
    {
      enabled: Boolean(userInfo.address),
    }
  )
  return (
    <React.Fragment>
      <Header></Header>
      <div className="px-8 tablet:px-20 desktop:px-40">
        {!userInfo.address ? (
          <div className="grid grid-cols-4 tablet:flex tablet:items-center tablet:mt-8">
            <div className="col-span-4 mb-4 text-center tablet:w-1/2 tablet:mb-0 tablet:text-left">
              <h3 className="headline-3">Login to view your items.</h3>
              <Link href="/" passHref>
                <Button className="mt-4" as="a">
                  Go Home
                </Button>
              </Link>
            </div>
            <div className="col-span-4 tablet:w-1/2 ">
              <Image alt="cta" src={ctaImage} layout="responsive" />
            </div>
          </div>
        ) : (
          <div>
            {isLoading ? (
              <div>Loading</div>
            ) : (
              <div className="grid grid-cols-1 gap-8 mt-8 tablet:grid-cols-3 desktop:grid-cols-4 ">
                {data.data.map((token) => {
                  return <CardItem key={token.id} token={token} />
                })}
              </div>
            )}
          </div>
        )}
      </div>
    </React.Fragment>
  )
}

export default ItemsPage
