import { ButtonBack } from '@/components/common/Button/ButtonBack'
import { Activity } from '@/components/shared/Activity/Activity'
import { Header } from '@/components/shared/Header/Header'
import React from 'react'

const ActivityPage = () => {
  return (
    <>
      <Header />
      <div className="p-8 border-b border-solid border-neutrals-6 tablet:px-20 desktop:px-40">
        <ButtonBack />
      </div>
      <Activity />
    </>
  )
}

export default ActivityPage
