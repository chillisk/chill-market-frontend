import React from 'react'
import Image from 'next/image'
import SVG from 'react-inlinesvg'
import { NextIcon, ArrowLeftIcon } from '@/assets/icons/icons'
import singleCube from '../assets/images/single-cube.png'
import multiCube from '../assets/images/multi-cube.png'
import singleRect from '../assets/images/single-rect.png'
import multiRect from '../assets/images/multi-rect.png'

import { Button } from '@/components/shared/Button/Button'
import { Header } from '@/components/shared/Header/Header'
import Link from 'next/link'

const UploadPage: React.FC = () => {
  return (
    <>
      <Header />
      <div className="pb-16 border-b border-solid border-neutrals-6">
        {/*  Nav */}
        <div className="px-8 py-6 mb-8 border-b border-solid tablet:mb-16 tablet:flex tablet:justify-between tablet:px-40 tablet:py-4 border-neutrals-6">
          <Button className="flex items-center justify-center">
            <span className="mr-4">
              <SVG
                className="w-4 h-4 text-neutrals-4"
                src={ArrowLeftIcon.src}
              />
            </span>
            <span>Back to home</span>
          </Button>
          <div className="hidden text-[0.875rem] leading-4 tablet:flex-row tablet:items-center tablet:justify-between tablet:flex">
            <span className="text-neutrals-4">Home</span>
            <SVG className="mx-8" src={NextIcon.src} />
            <span className="text-neutrals-2">Upload Item</span>
          </div>
        </div>

        {/* Hero */}
        <div className="px-8 mb-8 tablet:mb-16 tablet:px-40">
          <h2 className="mb-4 text-center headline-2 text-neutrals-2">
            Upload item
          </h2>
          <p className="px-2 text-center caption text-neutrals-4 tablet:px-0">
            Choose <span className="text-neutrals-2">“Single” </span> if you
            want your collectible to be one of a kind or{' '}
            <span className="text-neutrals-2">“Multiple” </span> if you want to
            sell one collectible multiple times
          </p>
        </div>

        {/* Upload */}
        <div className="px-8 mb-8 tablet:px-40 tablet:flex tablet:flex-row tablet:justify-center">
          {/* Create Single */}
          <div className="flex justify-between pb-8 mb-8 border-b border-solid tablet:p-4 tablet:mb-0 border-neutrals-6 tablet:border tablet:border-solid rounded-2xl tablet:mr-4">
            {/* Image Mobile */}
            <div className="flex items-center tablet:hidden">
              <Image src={singleCube} alt="single-cube" quality={100} />
              <span className="px-5 text-neutrals-2">Create Single</span>
            </div>
            <div className="flex items-center tablet:hidden">
              <SVG src={NextIcon.src} />
            </div>
            {/* Image Tablet, Desktop */}
            <div className="hidden tablet:flex-col tablet:flex">
              <div className="flex items-center justify-center order-1 pt-6">
                <Link href="/upload-single" passHref>
                  <a>
                    <Button>Create Single</Button>
                  </a>
                </Link>
              </div>
              <Image src={singleRect} alt="single-cube" quality={100} />
            </div>
          </div>
          {/* End Create Single */}

          {/* Create Multiple */}
          <div className="flex justify-between pb-8 border-b border-solid tablet:ml-4 border-neutrals-6 tablet:p-4 tablet:border tablet:border-solid rounded-2xl">
            {/* Image Mobile */}
            <div className="flex items-center tablet:hidden">
              <Image src={multiCube} alt="single-cube" quality={100} />
              <span className="px-5 text-neutrals-2">Create Single</span>
            </div>
            <div className="flex items-center tablet:hidden">
              <SVG src={NextIcon.src} />
            </div>
            {/* Image Tablet, Desktop */}
            <div className="hidden tablet:flex-col tablet:flex">
              <div className="flex items-center justify-center order-1 pt-6">
                <Button variant="neutral">Create Multiple</Button>
              </div>
              <Image src={multiRect} alt="single-cube" quality={100} />
            </div>
          </div>
          {/* End Create Multiple */}
        </div>

        {/* Upload footer */}
        <div className="px-8">
          <p className="px-8 text-center caption-2 text-neutrals-4">
            We do not own your private keys and cannot access your funds without
            your confirmation.
          </p>
        </div>
      </div>
    </>
  )
}

export default UploadPage
