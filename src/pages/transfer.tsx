import { Header } from '@/components/shared/Header/Header'
import { TransferContainer } from '@/containers/Transfer/Transfer'
import React from 'react'

const TransferPage = () => {
  return (
    <React.Fragment>
      <Header />
      <TransferContainer />
    </React.Fragment>
  )
}

export default TransferPage
