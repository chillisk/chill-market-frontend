import { Header } from '@/components/shared/Header/Header'
import {
  FormControl,
  FormHelperText,
  FormLabel,
  Input,
  Button,
  useToast,
} from '@chakra-ui/react'
import React from 'react'
import { useForm } from 'react-hook-form'
import IPFS from 'ipfs-http-client'
import { useMutation } from 'react-query'
import {
  CreateNFTParams,
  createNFTToken,
} from '@/utils/transactions/create-nft-token'
import { useAtom } from 'jotai'
import { nodeInfoAtom } from '@/atom/node-info'
import { sendTransactions } from '@/services/api'

const ipfs = IPFS({
  host: 'ipfs.infura.io',
  port: 5001,
  protocol: 'https',
})

const UploadSingleNFTPage = () => {
  const { register, handleSubmit, reset } = useForm()
  const [nodeInfo] = useAtom(nodeInfoAtom)
  const toast = useToast()
  const { mutate, isLoading } = useMutation(
    async (e: any) => {
      const buffer = await new Response(e.file[0]).arrayBuffer()
      const ipFile = await ipfs.add(buffer)
      const url = `https://gateway.ipfs.io/ipfs/${ipFile.path}`
      const data: CreateNFTParams = {
        passphrase: e.passphrase,
        name: e.name,
        url,
        minFeePerByte: nodeInfo.minFeePerByte,
        networkIdentifier: nodeInfo.networkIdentifier,
      }
      const res = await createNFTToken(data)
      const createResult = await sendTransactions(res.tx)
      return createResult
    },
    {
      onSuccess() {
        reset()
        toast({
          title: 'NFT Created.',
          status: 'success',
          duration: 9000,
          isClosable: true,
        })
      },
    }
  )

  function onSubmit(e: any) {
    mutate(e)
  }
  return (
    <React.Fragment>
      <Header></Header>
      <div className="px-8 tablet:px-20 desktop:px-40">
        <h2 className="mt-20 headline-2">Create single collectible</h2>
        <form className="mt-8 form" onSubmit={handleSubmit(onSubmit)}>
          <FormControl isRequired>
            <FormLabel>Upload a file</FormLabel>
            <Input type="file" {...register('file', { required: true })} />
            <FormHelperText>Drag or choose your file to upload</FormHelperText>
          </FormControl>
          <FormControl className="mt-4" isRequired>
            <FormLabel>Name</FormLabel>
            <Input
              placeholder="Enter your NFT name"
              {...register('name', { required: true })}
            />
          </FormControl>
          <FormControl className="mt-4" isRequired>
            <FormLabel>Passphrase</FormLabel>
            <Input
              placeholder="Enter your passphrase"
              {...register('passphrase', { required: true })}
            />
          </FormControl>
          <Button type="submit" className="mt-4" isLoading={isLoading}>
            Create
          </Button>
        </form>
      </div>
    </React.Fragment>
  )
}

export default UploadSingleNFTPage
